여기까지이다! 

길고 지루하지만 튜토리얼을 마치고 이제 컨테이너 세계를 폭풍으로 몰아넣을 준비가 되었다! 여기까지 잘 따라왔다면 스스로를 자랑스러워 하여도 충분하다. Docker를 설정하고, 자신만의 컨테이너를 실행하고, 정적 또는 동적 웹사이트를 다루는 방법을 배웠으며, 가장 중요한 것은 어플리케이션을 서버 또는 클라우드에 배포하는 경험을 직접해 본 것이다!

이 튜토리얼을 마치면서 서버를 다루는 능력에 더욱 자신감이 생기셨기를 바란다. 다음 앱을 개발할 아이디어가 있다면 최소한의 노력으로 사람들에게 앱을 선보일 수 있을 것이다.

## 다음 단계
컨테이너 세계로의 여정이 이제 막 시작되었다! 

이 튜토리얼의 목표는 여러분의 호기심을 자극하고 Docker의 강력한 기능을 보여주는 것이었다. 새로운 기술의 바다에서 혼자서 헤쳐나가는 것은 어려울 수 있으며, 이와 같은 튜토리얼이 도움이 될 수 있기를 바란다. 처음 시작할 때 이같은 튜토리얼이 있었으면 좋았겠지만, 이제 더 이상 옆에서 지켜볼 필요가 없도록 컨테이너에 대해 흥미를 갖는 데 도움이 되었기를 바란다.

다음은 도움이 될 몇몇 리소스들이다. 다음 프로젝트에서는 Docker를 사용해 보길 강력히 추천한다. 명심하세요 - 연습이 발전을 이룬다!

추가 리소스

- [Awesome Docker](https://github.com/veggiemonk/awesome-docker)
- [Why Docker](https://blog.codeship.com/why-docker/)
- [Docker Weekly](https://www.docker.com/newsletter-subscription)와 [archives](https://blog.docker.com/docker-weekly-archives/)
- [Codeship Blog](https://blog.codeship.com/)
