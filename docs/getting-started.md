이 튜토리얼은 여러 섹션으로 구성되어 있으며, 각 섹션은 Docker의 특정 측면을 설명한다. 각 섹션에서 명령을 입력(또는 코드 작성)할 것이다. 튜토리얼의 모든 코드는 [Github repo](http://github.com/prakhar1989/docker-curriculum)에서 저장되어 있다.

> **Note**: 이 튜토리얼에서는 Docker 버전 18.05.0-ce를 사용한다. 튜토리얼에서 향후 버전과 호환되지 않는 부분을 발견하면 [이슈](https://github.com/prakhar1989/docker-curriculum/issues)로 남겨 주세요. 감사합니다!

## 전제 조건(Prerequisites)
이 튜토리얼에는 명령어와 편집기에 대한 기본적인 익숙함 외에 특별한 기술이 필요하지 않다. 이 튜토리얼에서는 로컬로 리포지토리를 복제하기 위해 `git clone`을 사용한다. 시스템에 Git이 설치되어 있지 않은 경우 설치하거나 Github에서 zip 파일을 수동으로 다운로드해야 한다. 웹 어플리케이션 개발 경험이 있으면 도움이 되지만 꼭 필요한 것은 아니다. 튜토리얼을 진행하면서 클라우드 서비스를 사용할 예정이다. 웹사이트에서 계정을 만들고 따라 해보록 하자.
- [Amazon Web Services](http://aws.amazon.com/)
- [Docker Hub](https://hub.docker.com/)

## 컴퓨터 설정
컴퓨터에 모든 도구를 설정하는 것은 어려운 작업일 수 있지만, 다행히도 Docker가 안정화되면서 선호하는 OS에서 Docker를 실행하는 것이 매우 쉬워졌다.

몇 번의 릴리스 전까지만 해도 MacOS와 Windows에서 Docker를 실행하는 것은 상당히 번거로웠다. 하지만 최근 Docker는 이러한 OS에서 사용자의 온보딩 경험을 개선하기 위해 많은 투자를 해왔으며, 이제 Docker를 실행하는 것은 매우 쉬워졌다. Docker의 시작 가이드에는 [Mac](https://docs.docker.com/docker-for-mac/install), [Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu) 및 [Windows](https://docs.docker.com/docker-for-windows/install)에서 Docker를 설정하는 자세한 지침이 실려있다.

Docker 설치를 완료했으면 다음을 실행하여 Docker 설치를 테스트할 수 있다.

```bash
$ docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```

