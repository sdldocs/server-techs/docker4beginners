## Busybox로 플레이하기
이제 모든 설정이 완료되었으니, 이제 삽질을 시작할 차례이다. 이 섹션에서는 시스템에서 Busybox 컨테이너를 실행하고 docker 실행 명령을 사용해 보겠다.

시작하려면 터미널에서 다음을 실행한다.

```bash
$ docker pull busybox
```

> **Note**: 시스템에 도커를 설치한 방법에 따라 위 명령을 실행했을 때 `permission denied`가 나타날 수 있다. Mac을 사용하는 경우 Docker 엔진이 실행 중인지 확인한다. Linux를 사용하는 경우 `docker` 명령 앞에 `sudo`를 추가한다. 또는 [docker 그룹을 만들어](https://docs.docker.com/engine/installation/linux/linux-postinstall/) 이 문제를 해결할 수 있다.

`pull` 명령은 [Docker registry](https://hub.docker.com/explore/)에서 busybox [image](https://hub.docker.com/_/busybox/)를 가져와 시스템에 저장한다. `docker images` 명령을 사용하여 시스템의 모든 이미지 목록을 볼 수 있다.

```bash
$ docker pull busybox
REPOSITORY    TAG       IMAGE ID       CREATED         SIZE
busybox       latest    3fbc63216742   5 seconds ago   6.09MB
```

## Docker Run
멋지네요! 이제 이 이미지를 기반으로 Docker 컨테이너를 실행해 보자. 이를 위해 전능한 `docker run` 명령을 사용하겠다.

```bash
$ docker run busybox
$
```

잠깐, 아무 일도 일어나지 않았다! 버그인가? 아니요. 뒤에서 많은 일이 일어났다. `run`을 호출하면 Docker 클라이언트가 이미지(이 경우 busybox)를 찾아 컨테이너를 로드한 다음 해당 컨테이너에서 명령을 실행한다. `docker run busybox`를 실행할 때 명령을 제공하지 않았기 때문에 컨테이너가 부팅되고 빈 명령을 실행한 다음 종료되었다. 좀 아쉽네요. 좀 더 흥미로운 것을 시도해 보자.

```bash
$ docker run busybox echo "hello from busybox"
hello from busybox
```

드디어 무엇인가 출력되었다. 이 경우, Docker 클라이언트는 busybox 컨테이너에서 `echo` 명령을 성실하게 실행한 다음 종료했다. 눈치채겠지만, 이 모든 일이 매우 빠르게 진행되었다. 가상 머신을 부팅하고 명령을 실행한 다음 종료한다고 상상해 볼 떄 컨테이너가 빠르다고 말하는 이유를 이제 알 수 있을 것이다! `docker ps` 명령을 살펴볼 차례이다. `docker ps` 명령은 현재 실행 중인 모든 컨테이너를 보여준다.

```bash
$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

실행 중인 컨테이너가 없으므로 빈 줄이 표시되었다. `docker ps -a` 명령으로 좀 더 유용한 변경을 시도해 보자.

```bash
$ docker ps -a
CONTAINER ID   IMAGE         COMMAND                   CREATED         STATUS                     PORTS     NAMES
1fa6d6d0f508   busybox       "echo 'hello from bu…"   4 minutes ago   Exited (0) 4 minutes ago             condescending_noether
38a538df80fc   busybox       "sh"                      4 minutes ago   Exited (0) 4 minutes ago             confident_meninsky
6df172d7c000   hello-world   "/hello"                  5 minutes ago   Exited (0) 5 minutes ago             infallible_haibt
```

위에 출력된 것은 실행 중인 모든 컨테이너의 목록이다. `STATUS` 열에 몇 분 전에 이 컨테이너들이 종료되었음을 알 수 있다.

컨테이너에서 하나 이상의 명령을 실행할 수 있는 방법이 있다. 지금 시도해 보자.

```bash
$ docker run -it busybox sh
/ # ls
bin    dev    etc    home   lib    lib64  proc   root   sys    tmp    usr    var
/ # uptime
 07:31:16 up  9:26,  0 users,  load average: 0.37, 0.09, 0.03
```

`-it` 플래그를 사용하여 `run` 명령을 실행하면 컨테이너의 대화형 tty에 연결된다. 이제 컨테이너에서 원하는 만큼의 명령을 실행할 수 있다. 시간을 내어 좋아하는 명령을 실행해 보도록 한다.

> **Danger Zone**: 특히 모험심이 강하다면 컨테이너에서 `rm -rf bin` 명령을 사용해 볼 수 있다. 이 명령은 노트북/데스크톱이 아닌 컨테이너에서 실행해야 한다. 이렇게 하면 `ls`, `uptime`과 같은 명령들이 동작하지 않는다. 모든 것이 작동을 멈추면 컨테이너를 종료한 다음(`exit`를 입력하고 Enter 키를 누름) ₩docker run -it busybox sh₩ 명령으로 컨테이너를 다시 시작할 수 있다. Docker는 매번 새 컨테이너를 생성하므로 모든 것이 다시 작동한다.

이것으로 가장 자주 사용하게 될 강력한 `docker run` 명령에 대한 간략한 소개를 마친다. 이 명령에 익숙해지는 데 시간을 투자하는 것이 좋다. `run`에 대해 자세히 알아보려면 `docker run --help`를 사용하여 지원하는 모든 플래그 목록을 확인할 수 있다. 더 진행하면서 `docker run` 명령의 몇몇 변형을 더 보게 될 것이다.

하지만 계속 진행하기 전에 컨테이너 삭제에 대해 간단히 알아보자. 위에서 `docker ps -a`를 실행한 다음 종료한 후에도 컨테이너의 잔여물을 볼 수 있음을 확인했다. 이 튜토리얼을 진행하는 동안 `docker run` 명령을 여러 번 실행하게 되며, 종료된 컨테이너를 남겨두면 디스크 공간을 차지하게 된다. 따라서 일반적으로 작업을 마치면 컨테이너를 정리하는 것이 바람직 하다. 이를 위해 `docker rm` 명령을 실행하면 된다. 위에서 컨테이너 ID를 복사하여 명령어와 함께 붙여넣기만 하면 된다.

```bash
$ docker rm 1fa6d6d0f508 38a538df80fc
1fa6d6d0f508
38a538df80fc
```

삭제하면 ID가 다시 에코되는 것을 볼 수 있다. 한 번에 삭제할 컨테이너가 많은 경우 ID를 복사하여 붙여넣는 작업이 번거로울 수 있다. 이 경우 간단히 다음 명령을 실행하면 된다.

```bash
$ docker rm $(docker ps -a -q -f status=exited)
```

이 명령은 `exited` 상태인 모든 컨테이너를 삭제한다. 궁금하신 경우, `-q` 플래그는 숫자 ID만 반환하고 `-f`는 제공된 조건에 따라 출력을 필터링한다. 마지막으로 유용한 것은 컨테이너가 종료되면 컨테이너를 자동으로 삭제하는 `docker run`에 전달할 수 있는 `--rm` 플래그이다. 일회성 도커 실행의 경우 `--rm` 플래그가 매우 유용하다.

이후 버전의 Docker에서는 `docker container prune` 명령을 사용하여 동일한 효과를 얻을 수 있다.

```bash
$ docker container prune
WARNING! This will remove all stopped containers.
Are you sure you want to continue? [y/N] y
Deleted Containers:
6df172d7c0005faf64969da84581616cdf87e63e37b7f4ad6cd0dbd8c3e65ae6

Total reclaimed space: 20.48kB
```

마지막으로, 더 이상 필요하지 않은 이미지는 `docker rmi` 명령을 실행하여 삭제할 수 있다.

## 용어
지난 섹션에서는 혼란스러울 수 있는 Docker 관련 전문 용어를 많이 사용했다. 계속 진행하기 전에 Docker 생태계에서 자주 사용되는 몇몇 용어들을 명확히 설명한다.

- *이미지(Images)* - 컨테이너의 기초가 되는 어플리케이션의 청사진이다. 위의 데모에서는 `docker pull` 명령을 사용하여 busybox 이미지를 다운로드했다.
- *컨테이너(container)* - Docker 이미지에서 생성되어 실제 어플리케이션을 실행한다. 다운로드한 busybox 이미지를 사용하여 `docker run` 명령을 수행하여 컨테이너를 생성한다. 실행 중인 컨테이너 목록은 `docker ps` 명령으로 확인할 수 있다.
- *Docker 데몬* - 호스트에서 실행되는 백그라운드 서비스로, Docker 컨테이너 빌드, 실행 및 배포를 관리한다. 데몬은 클라이언트가 통신하는 운영 체제에서 실행되는 프로세스이다.
- *Docker 클라이언트* - 사용자가 데몬과 상호 작용할 수 있도록 지원하는 명령어 도구이다. 더 일반적으로, 사용자에게 GUI를 제공하는 [Kitematic](https://kitematic.com/)과 같은 다른 형태의 클라이언트도 있다.
- *Docker Hub* - Docker 이미지의 [레지스트리](https://hub.docker.com/explore/). 레지스트리는 사용 가능한 모든 Docker 이미지의 디렉토리로 생각할 수 있다. 필요한 경우 자체 Docker 레지스트리를 호스팅하여 이미지를 가져오는 데 사용할 수 있다.
