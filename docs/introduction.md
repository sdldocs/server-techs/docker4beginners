## Docker란?
Wikipedia에서는 [Docker](https://www.docker.com/)를 다음과 같이 정의하고 있다.

> Linux에서 OS 수준 가상화를 추상화와 자동화하는 추가 계층을 제공하여 컨테이너 내부의 소프트웨어 어플리케이션 배포를 자동화하는 오픈 소스 프로젝트이다.

와우! 정말 어렵네. 간단히 말해, Docker는 개발자, 시스템 관리자 등이 어플리케이션을 샌드박스(*컨테이너*라고 함)에 쉽게 배포하여 호스트 운영 체제(예: Linux)에서 실행할 수 있도록 하는 도구이다. Docker의 주요 이점은 사용자가 모든 종속성이 있는 어플리케이션을 소프트웨어 개발을 위한 표준화된 단위로 패키징할 수 있다는 것이다. 가상 머신과 달리 컨테이너는 오버헤드가 높지 않으므로 기본 시스템과 리소스를 보다 효율적으로 사용할 수 있다.

## 컨테이너란?
오늘날 업계 표준은 가상 머신(VM)을 사용하여 소프트웨어 어플리케이션을 실행하는 것이다. VM은 서버의 호스트 OS가 구동하는 가상 하드웨어에서 실행되는 게스트 운영 체제 내에서 어플리케이션을 실행한다.

VM은 어플리케이션에 대한 완전하게 프로세스를 격리하는 탁월한 기능을 제공한다. 호스트 운영 체제의 문제가 게스트 운영 체제에서 실행되는 소프트웨어에 영향을 미치거나 그 반대의 경우도 거의 발생하지 않는다. 그러나 이러한 격리에는 막대한 비용이 발생하며, 게스트 OS가 사용할 하드웨어를 가상화하는 데 소요되는 컴퓨팅 오버헤드가 상당하다.

컨테이너는 호스트 운영 체제의 하위 수준의 메커니즘을 활용하여 적은 컴퓨팅 파워로 가상 머신의 격리 기능을 대부분 제공한다.

## 컨테이너를 사용하는 이유
컨테이너는 어플리케이션이 실제로 실행되는 환경으로부터 어플리케이션을 추상화할 수 있는 논리적 패키징 메커니즘을 제공한다. 이러한 분리를 통해 대상 환경이 프라이빗 데이터 센터, 퍼블릭 클라우드, 심지어 개발자의 개인 노트북이든 상관없이 컨테이너 기반 어플리케이션을 쉽고 일관성 있게 배포할 수 있다. 이를 통해 개발자는 나머지 어플리케이션과 분리되어 어디서나 실행할 수 있는 예측 가능한 환경을 만들 수 있다.

운영 측면에서도 이동성 외에도 컨테이너는 리소스를 보다 세밀하게 제어할 수 있어 인프라의 효율성을 높여 컴퓨팅 리소스의 활용도를 높일 수 있다.

![](images/introductiom/interest.webp)

이러한 장점으로 인해 컨테이너(및 Docker)는 널리 채택되고 있다. Google, Facebook, Netflix, Salesforce와 같은 기업에서는 컨테이너를 활용하여 대규모 엔지니어링 팀의 생산성을 높이고 컴퓨팅 리소스 활용도를 개선하고 있다. 실제로 Google은 컨테이너 덕분에 전체 데이터센터가 필요하지 않게 되었다고 한다.

## 이 튜토리얼에서 무엇을 배울 수 있나?
이 튜토리얼은 Docker를 직접 사용해 볼 수 있는 원스톱 쇼핑을 목표로 합니다. Docker 환경을 이해하는 것 외에도 클라우드에서 자체 웹앱을 빌드하고 배포하는 실습 경험을 제공한다. [Amazon Web Services](http://aws.amazon.com/)를 사용하여 정적 웹사이트를 배포하고, [Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/)와 [Elastic Container Service](https://aws.amazon.com/ecs/)를 사용하여 [EC2](https://aws.amazon.com/ec2/)에 두 개의 동적 웹앱을 배포할 것이다. 배포에 대한 사전 경험이 없더라도 이 튜토리얼을 통해 시작하는 데 필요한 모든 것을 얻을 수 있다.
