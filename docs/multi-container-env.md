지난 장에서는 Docker로 어플리케이션을 실행하는 것이 얼마나 쉽고 편한지를 살펴보았다. 간단한 정적 웹사이트로 시작하여 Flask 앱을 사용했다. 두 앱 모두 몇 가지 명령만으로 로컬과 클라우드에서 실행할 수 있었다. 이 두 앱의 공통점은 단일 컨테이너에서 실행된다는 것이다.

프로덕션 환경에서 서비스를 실행해 본 경험이 있는 분들은 요즘의 앱이 그렇게 간단하지 않다는 것을 알고 있을 것이다. 거의 항상 데이터베이스(또는 다른 종류의 영구 저장소)가 사용되고 있다. [Redis](http://redis.io/)나 [Memcached](http://memcached.org/)와 같은 시스템은 대부분의 웹 어플리케이션 아키텍처의 표준이 되었다. 따라서 이 장에서는 다양한 서비스에 의존하는 어플리케이션을 실행하기 위해 도커라이즈하는 방법에 대해 알아본다.

특히 다중 컨테이너 Docker 환경을 어떻게 실행하고 관리할 수 있는지 살펴보겠다. 왜 다중 컨테이너일까요? Docker의 핵심 포인트 중 하나는 격리 기능을 제공하는 방식이다. 프로세스와 그 종속성을 샌드박스(컨테이너라고 함)에 번들로 묶는 아이디어가 바로 이 강력한 기능을 제공한다.

어플리케이션 티어를 분리하는 것이 좋은 전략인 것처럼, 각 서비스에 대한 컨테이너를 분리할 수 있다. 각 티어마다 필요한 리소스가 다를 수 있으며 이러한 리소스는 서로 다른 속도로 증가할 수 있다. 티어를 서로 다른 컨테이너로 분리하면 리소스 요구 사항에 따라 가장 적합한 인스턴스 유형을 사용하여 각 티어를 구성할 수 있다. 이는 또한 전체 마이크로서비스 움직임과도 잘 맞아떨어진다. 이는 Docker(또는 다른 컨테이너 기술)가 최신 [마이크로서비스](http://martinfowler.com/articles/microservices.html) 아키텍처의 [최전선](https://medium.com/aws-activate-startup-blog/using-containers-to-build-a-microservices-architecture-6e1b8bacb7d1#.xl3wryr5z)인 주된 이유 중 하나이다.

## SF Food Trucks
우리가 도커라이즈할 앱의 이름은 SF Food Trucks이다. 이 앱을 구축할 때 목표는 (실제 어플리케이션과 유사하다는 점에서) 유용하고, 적어도 하나의 서비스에 의존하지만 이 튜토리얼의 목적에 비해 너무 복잡하지 않은 앱을 만드는 것이다. 

![](images/multi-container-env/foodtrucks.webp)

앱의 백엔드는 Python(Flask)으로 작성되었으며 검색에는 [Elasticsearch](https://www.elastic.co/products/elasticsearch)를 사용한다. 이 튜토리얼의 다른 모든 내용과 마찬가지로 전체 소스는 [Github](http://github.com/prakhar1989/FoodTrucks)에 있다. 다중 컨테이너 환경을 빌드, 실행과 배포하는 방법을 배우기 위한 어플리케이션으로 이 앱을 사용한다.

먼저 리포지토리를 로컬로 복제한다.

```bash
$ git clone https://github.com/prakhar1989/FoodTrucks
$ cd FoodTrucks
$ tree -L 2
.
├── Dockerfile
├── README.md
├── aws-ecs
│   └── docker-compose.yml
├── docker-compose.yml
├── flask-app
│   ├── app.py
│   ├── package-lock.json
│   ├── package.json
│   ├── requirements.txt
│   ├── static
│   ├── templates
│   └── webpack.config.js
├── setup-aws-ecs.sh
├── setup-docker.sh
├── shot.png
├── utils
│   ├── generate_geojson.py
│   └── trucks.geojson
└── venv
    ├── bin
    ├── lib
    └── pyvenv.cfg

9 directories, 15 files
```

`flask-app` 폴더에는 Python 어플리케이션이 있고, `utils` 폴더에는 `Elasticsearch`로 데이터를 로드하기 위한 몇 가지 유틸리티가 있다. 이 디렉터리에는 또한 일부 YAML 파일과 Dockerfile이 포함되어 있으며, 이 튜토리얼을 진행하면서 더 자세히 살펴보게 될 것이다. 궁금하신 점이 있으시면 언제든지 파일을 살펴보길 바란다.

이제 앱을 도커화(dockerize)할 수 있는 방법을 생각해 보자. 어플리케이션이 Flask 백엔드 서버와 Elasticsearch 서비스로 구성되어 있음을 알 수 있다. 이 앱을 자연스럽게 분할하는 방법은 두 개의 컨테이너, 즉 하나는 Flask 프로세스를 실행하는 컨테이너이고 다른 하나는 Elasticsearch(ES) 프로세스를 실행하는 컨테이너이다. 이렇게 하면 앱이 인기를 얻으면 병목 현상이 발생하는 위치에 따라 컨테이너를 더 추가하여 확장할 수 있다.

좋아요, 그럼 컨테이너가 두 개 필요하다. 어렵지 않겠지? 이전 장에서 이미 자체 Flask 컨테이너를 구축했다. 그리고 Elasticsearch의 경우, 허브에서 무언가를 찾을 수 있는지 살펴보자.

```bash
$ docker search elasticsearch
NAME                                     DESCRIPTION                                       STARS     OFFICIAL   AUTOMATED
elasticsearch                            Elasticsearch is a powerful open source sear…    6121      [OK]       
kibana                                   Kibana gives shape to any kind of data — str…   2626      [OK]       
bitnami/elasticsearch                    Bitnami Docker Image for Elasticsearch            67                   [OK]
bitnami/elasticsearch-exporter           Bitnami Elasticsearch Exporter Docker Image       7                    [OK]
rancher/elasticsearch-conf                                                                 2                    
rapidfort/elasticsearch                  RapidFort optimized, hardened image for Elas…    10                   
bitnami/elasticsearch-curator-archived   A copy of the container images of the deprec…    0                    
rapidfort/elasticsearch-official         RapidFort optimized, hardened image for Elas…    0                    
bitnamicharts/elasticsearch                                                                0                    
onlyoffice/elasticsearch                                                                   1                    
rancher/elasticsearch                                                                      1                    
couchbase/elasticsearch-connector        Couchbase Elasticsearch Connector                 0                    
rancher/elasticsearch-bootstrap                                                            1                    
dtagdevsec/elasticsearch                 T-Pot Elasticsearch                               4                    [OK]
corpusops/elasticsearch                  https://github.com/corpusops/docker-images/       0                    
vulhub/elasticsearch                                                                       0                    
uselagoon/elasticsearch-7                                                                  0                    
securecodebox/elasticsearch                                                                0                    
eucm/elasticsearch                       Elasticsearch 1.7.5 Docker Image                  1                    [OK]
ilios/elasticsearch                                                                        0                    
uselagoon/elasticsearch-6                                                                  0                    
openup/elasticsearch-0.90                                                                  0                    
litmuschaos/elasticsearch-stress                                                           0                    
drud/elasticsearch_exporter                                                                0                    
geekzone/elasticsearch-curator                                                             0                    
```

놀랍지 않게도, 공식적으로 지원되는 Elasticsearch용 [이미지](https://store.docker.com/images/elasticsearch)가 있다. ES를 실행하려면 간단히 `docker run`을 사용하여 단일 노드 ES 컨테이너를 로컬에서 즉시 실행할 수 있다.

> **Note**: Elasticsearch의 개발사인 Elastic은 Elastic 제품에 대한 [자체 레지스트리](https://www.docker.elastic.co/)를 유지 관리한다. Elasticsearch를 사용하려는 경우 해당 레지스트리의 이미지를 사용하는 것이 바람직하다.

먼저 이미지를 가져오자.

```bash
$ docker pull docker.elastic.co/elasticsearch/elasticsearch:6.3.2
6.3.2: Pulling from elasticsearch/elasticsearch
7dc0dca2b151: Pull complete
72d60ff53590: Pull complete
ca55c9f7cc1f: Pull complete
822d6592a660: Pull complete
22eceb1ece84: Pull complete
30e73cf19e42: Pull complete
f05e800ca884: Pull complete
3e6ee2f75301: Pull complete
Digest: sha256:8f06aecf7227dbc67ee62d8d05db680f8a29d0296ecd74c60d21f1fe665e04b0
Status: Downloaded newer image for docker.elastic.co/elasticsearch/elasticsearch:6.3.2
docker.elastic.co/elasticsearch/elasticsearch:6.3.2
```

를 설치한 다음 포트를 지정하고 단일 노드로 실행되도록 Elasticsearch 클러스터를 구성하는 환경 변수를 설정하여 개발 모드에서 실행한다.

```bash
$ docker run -d --name es -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node"
 docker.elastic.co/elasticsearch/elasticsearch:6.3.2
6b9e70a4ca46366dae38b6ab2616d5769217b1ac6d447d8c45f99f080e474cf2
```

> **Note**: 컨테이너에서 메모리 문제가 발생하는 경우, 메모리 소비를 제한하기 위해 일부 JVM 플래그를 조정해야 할 수 있다.

> 2023년 7월 26알 현재 elasticsearch는 8.9가 배포되고 있으나 8.0 이후 보안정책이 강화되었다. ([Start a single-node cluster with Docker
](https://www.elastic.co/guide/en/elasticsearch/reference/8.0/docker.html#docker-cli-run-dev-mode) 참조하세요.) 따라서 version 6.3.2를 사용하였다.

위에서 보았듯이 `--name es`를 사용하여 컨테이너에 이름을 지정하여 후속 명령에서 쉽게 사용할 수 있도록 한다. 컨테이너가 시작되면, 컨테이너 이름(또는 ID)으로 `docker xontainer log`를 실행하여 로그를 검사하면 로그를 볼 수 있다. Elasticsearch가 성공적으로 시작되었다면 아래와 유사한 로그가 디스플레이된다.

> **Note**: Elasticsearch를 시작하는 데 몇 초가 걸리므로 로그에서 `initiaized` 확인을 위하여 기다려야 할 수도 있다.

```bash
$ container ls
CONTAINER ID   IMAGE                                                 COMMAND                  CREATED          STATUS          PORTS                                                                                  NAMES
6b9e70a4ca46   docker.elastic.co/elasticsearch/elasticsearch:6.3.2   "/usr/local/bin/dock…"   18 seconds ago   Up 17 seconds   0.0.0.0:9200->9200/tcp, :::9200->9200/tcp, 0.0.0.0:9300->9300/tcp, :::9300->9300/tcp   es
```

```bash
$ docker container logs es
OpenJDK 64-Bit Server VM warning: Option UseConcMarkSweepGC was deprecated in version 9.0 and will likely be removed in a future release.
[2023-07-26T08:40:41,133][INFO ][o.e.n.Node               ] [] initializing ...
[2023-07-26T08:40:41,204][INFO ][o.e.e.NodeEnvironment    ] [59LgCW6] using [1] data paths, mounts [[/ (overlay)]], net usable_space [51.2gb], net total_space [58.4gb], types [overlay]
[2023-07-26T08:40:41,205][INFO ][o.e.e.NodeEnvironment    ] [59LgCW6] heap size [1007.3mb], compressed ordinary object pointers [true]
[2023-07-26T08:40:41,208][INFO ][o.e.n.Node               ] [59LgCW6] node name derived from node ID [59LgCW69S8-WVBnho2U-uw]; set [node.name] to override
[2023-07-26T08:40:41,208][INFO ][o.e.n.Node               ] [59LgCW6] version[6.3.2], pid[1], build[default/tar/053779d/2018-07-20T05:20:23.451332Z], OS[Linux/5.10.47-linuxkit/amd64], JVM["Oracle Corporation"/OpenJDK 64-Bit Server VM/10.0.2/10.0.2+13]
[2023-07-26T08:40:41,208][INFO ][o.e.n.Node               ] [59LgCW6] JVM arguments [-Xms1g, -Xmx1g, -XX:+UseConcMarkSweepGC, -XX:CMSInitiatingOccupancyFraction=75, -XX:+UseCMSInitiatingOccupancyOnly, -XX:+AlwaysPreTouch, -Xss1m, -Djava.awt.headless=true, -Dfile.encoding=UTF-8, -Djna.nosys=true, -XX:-OmitStackTraceInFastThrow, -Dio.netty.noUnsafe=true, -Dio.netty.noKeySetOptimization=true, -Dio.netty.recycler.maxCapacityPerThread=0, -Dlog4j.shutdownHookEnabled=false, -Dlog4j2.disable.jmx=true, -Djava.io.tmpdir=/tmp/elasticsearch.ui9atnTe, -XX:+HeapDumpOnOutOfMemoryError, -XX:HeapDumpPath=data, -XX:ErrorFile=logs/hs_err_pid%p.log, -Xlog:gc*,gc+age=trace,safepoint:file=logs/gc.log:utctime,pid,tags:filecount=32,filesize=64m, -Djava.locale.providers=COMPAT, -XX:UseAVX=2, -Des.cgroups.hierarchy.override=/, -Des.path.home=/usr/share/elasticsearch, -Des.path.conf=/usr/share/elasticsearch/config, -Des.distribution.flavor=default, -Des.distribution.type=tar]
[2023-07-26T08:40:43,322][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [aggs-matrix-stats]
[2023-07-26T08:40:43,322][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [analysis-common]
[2023-07-26T08:40:43,323][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [ingest-common]
[2023-07-26T08:40:43,323][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [lang-expression]
[2023-07-26T08:40:43,323][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [lang-mustache]
[2023-07-26T08:40:43,324][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [lang-painless]
[2023-07-26T08:40:43,324][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [mapper-extras]
[2023-07-26T08:40:43,324][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [parent-join]
[2023-07-26T08:40:43,324][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [percolator]
[2023-07-26T08:40:43,324][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [rank-eval]
[2023-07-26T08:40:43,325][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [reindex]
[2023-07-26T08:40:43,325][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [repository-url]
[2023-07-26T08:40:43,325][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [transport-netty4]
[2023-07-26T08:40:43,325][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [tribe]
[2023-07-26T08:40:43,326][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-core]
[2023-07-26T08:40:43,326][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-deprecation]
[2023-07-26T08:40:43,326][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-graph]
[2023-07-26T08:40:43,326][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-logstash]
[2023-07-26T08:40:43,327][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-ml]
[2023-07-26T08:40:43,327][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-monitoring]
[2023-07-26T08:40:43,327][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-rollup]
[2023-07-26T08:40:43,328][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-security]
[2023-07-26T08:40:43,328][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-sql]
[2023-07-26T08:40:43,328][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-upgrade]
[2023-07-26T08:40:43,329][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded module [x-pack-watcher]
[2023-07-26T08:40:43,330][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded plugin [ingest-geoip]
[2023-07-26T08:40:43,330][INFO ][o.e.p.PluginsService     ] [59LgCW6] loaded plugin [ingest-user-agent]
[2023-07-26T08:40:46,022][INFO ][o.e.x.s.a.s.FileRolesStore] [59LgCW6] parsed [0] roles from file [/usr/share/elasticsearch/config/roles.yml]
[2023-07-26T08:40:46,702][INFO ][o.e.x.m.j.p.l.CppLogMessageHandler] [controller/79] [Main.cc@109] controller (64 bit): Version 6.3.2 (Build 903094f295d249) Copyright (c) 2018 Elasticsearch BV
[2023-07-26T08:40:47,473][INFO ][o.e.d.DiscoveryModule    ] [59LgCW6] using discovery type [single-node]
[2023-07-26T08:40:48,199][INFO ][o.e.n.Node               ] [59LgCW6] initialized
[2023-07-26T08:40:48,199][INFO ][o.e.n.Node               ] [59LgCW6] starting ...
[2023-07-26T08:40:48,405][INFO ][o.e.t.TransportService   ] [59LgCW6] publish_address {172.17.0.2:9300}, bound_addresses {0.0.0.0:9300}
[2023-07-26T08:40:48,464][INFO ][o.e.x.s.t.n.SecurityNetty4HttpServerTransport] [59LgCW6] publish_address {172.17.0.2:9200}, bound_addresses {0.0.0.0:9200}
[2023-07-26T08:40:48,465][INFO ][o.e.n.Node               ] [59LgCW6] started
[2023-07-26T08:40:48,566][WARN ][o.e.x.s.a.s.m.NativeRoleMappingStore] [59LgCW6] Failed to clear cache for realms [[]]
[2023-07-26T08:40:48,665][INFO ][o.e.g.GatewayService     ] [59LgCW6] recovered [0] indices into cluster_state
[2023-07-26T08:40:48,844][INFO ][o.e.c.m.MetaDataIndexTemplateService] [59LgCW6] adding template [.watch-history-7] for index patterns [.watcher-history-7*]
[2023-07-26T08:40:48,885][INFO ][o.e.c.m.MetaDataIndexTemplateService] [59LgCW6] adding template [.triggered_watches] for index patterns [.triggered_watches*]
[2023-07-26T08:40:48,912][INFO ][o.e.c.m.MetaDataIndexTemplateService] [59LgCW6] adding template [.watches] for index patterns [.watches*]
[2023-07-26T08:40:48,950][INFO ][o.e.c.m.MetaDataIndexTemplateService] [59LgCW6] adding template [.monitoring-logstash] for index patterns [.monitoring-logstash-6-*]
[2023-07-26T08:40:49,005][INFO ][o.e.c.m.MetaDataIndexTemplateService] [59LgCW6] adding template [.monitoring-es] for index patterns [.monitoring-es-6-*]
[2023-07-26T08:40:49,037][INFO ][o.e.c.m.MetaDataIndexTemplateService] [59LgCW6] adding template [.monitoring-alerts] for index patterns [.monitoring-alerts-6]
[2023-07-26T08:40:49,065][INFO ][o.e.c.m.MetaDataIndexTemplateService] [59LgCW6] adding template [.monitoring-beats] for index patterns [.monitoring-beats-6-*]
[2023-07-26T08:40:49,099][INFO ][o.e.c.m.MetaDataIndexTemplateService] [59LgCW6] adding template [.monitoring-kibana] for index patterns [.monitoring-kibana-6-*]
[2023-07-26T08:40:49,147][INFO ][o.e.l.LicenseService     ] [59LgCW6] license [ba304b57-0a36-43f7-a661-10b9bdf75e61] mode [basic] - valid
```

이제 Elasticsearch 컨테이너에 요청을 보낼 수 있는지 확인해 보자. `9200` 포트를 사용하여 컨테이너에 `cURL` 요청을 보낸다.

```bash
$ curl 0.0.0.0:9200
{
  "name" : "59LgCW6",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "NYn3IzDcR0ywQKMk07m2cA",
  "version" : {
    "number" : "6.3.2",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "053779d",
    "build_date" : "2018-07-20T05:20:23.451332Z",
    "build_snapshot" : false,
    "lucene_version" : "7.3.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```

멋지네! 좋아 보인다! 이왕 하는 김에 Flask 컨테이너도 실행해 보자. 하지만 그 전에 `Dockerfile`이 필요하다. 지난 섹션에서는 기본 이미지로 `python:3.8` 이미지를 사용했다. 하지만 이번에는 `pip`을 통해 파이썬 종속 요소를 설치하는 것 외에도 어플리케이션이 프로덕션을 위한 축소된 자바스크립트 파일도 생성하도록 하려고 한다. 이를 위해서는 Nodejs가 필요하다. 커스텀 빌드 단계가 필요하므로, `ubuntu` 기본 이미지에서 시작하여 처음부터 `Dockerfile`을 빌드하겠다.

> **Note**: 기존 이미지가 필요에 적합하지 않는 경우 다른 기본 이미지에서 시작하도록 직접 조정할 수 있다. Docker Hub에 있는 대부분의 이미지의 경우 Github에서 해당 `Dockerfile`을 찾을 수 있다. 기존 Dockerfiles을 읽어보는 것은 직접 롤링하는 방법을 배우는 가장 좋은 방법 중 하나이다.

Flask 앱용 [Dockerfile](https://github.com/prakhar1989/FoodTrucks/blob/master/Dockerfile)은 다음과 같다.

```dockerfile
# start from base
FROM ubuntu:18.04

MAINTAINER Prakhar Srivastav <prakhar@prakhar.me>

# install system-wide deps for python and node
RUN apt-get -yqq update
RUN apt-get -yqq install python-pip python-dev curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash
RUN apt-get install -yq nodejs

# copy our application code
ADD flask-app /opt/flask-app
WORKDIR /opt/flask-app

# fetch app specific deps
RUN npm install
RUN npm run build
RUN pip install -r requirements.txt

# expose port
EXPOSE 5000

# start app
CMD [ "python", "./app.py" ]
```

여기에는 꽤 많은 새로운 기능이 있으므로 이 파일을 빠르게 살펴본다. 먼저 [Ubuntu LTS](https://wiki.ubuntu.com/LTS)를 기본 이미지로 시작하여 패키지 관리자 `apt-get`을 사용하여 종속 요소, 즉 Python과 Node를 설치한다. `yqq` 플래그는 출력을 억제하는 데 사용되며 모든 프롬프트를 "예"로 가정gks다.

그런 다음 `ADD` 명령을 사용하여 어플리케이션을 컨테이너의 새 볼륨인 `/opt/flask-app`에 복사한다. 여기에 코드가 저장된다. 또한 이 위치를 작업 디렉터리로 설정하여 다음 명령이 이 위치의 컨텍스트에서 실행되도록 한다. 이제 시스템 전체 종속성이 설치되었으므로 앱별 종속성을 설치할 차례이다. 먼저 npm에서 패키지를 설치하고 `package.json` [파일](https://github.com/prakhar1989/FoodTrucks/blob/master/flask-app/package.json#L7-L9)에 정의된 대로 빌드 명령을 실행하여 Node를 다룬다. 파이썬 패키지를 설치하고, 포트를 노출하고, 마지막 섹션에서 한 것처럼 실행할 `CMD`를 정의하여 파일을 마무리한다.

마지막으로 이미지를 빌드하고 컨테이너를 실행할 수 있습니다(아래에서 `yourusername`을 `username`으로 바꾼다).

```bash
$ docker build -t yjbenlee/foodtrucks-web .
[+] Building 2.3s (16/16) FINISHED
 => [internal] load build definition from Dockerfile                                                                                                                                     0.0s
 => => transferring dockerfile: 605B                                                                                                                                                     0.0s
 => [internal] load .dockerignore                                                                                                                                                        0.0s
 => => transferring context: 2B                                                                                                                                                          0.0s
 => [internal] load metadata for docker.io/library/ubuntu:18.04                                                                                                                          2.1s
 => [auth] library/ubuntu:pull token for registry-1.docker.io                                                                                                                            0.0s
 => [internal] load build context                                                                                                                                                        0.1s
 => => transferring context: 3.68MB                                                                                                                                                      0.1s
 => [ 1/10] FROM docker.io/library/ubuntu:18.04@sha256:152dc042452c496007f07ca9127571cb9c29697f42acbfad72324b2bb2e43c98                                                                  0.0s
 => CACHED [ 2/10] RUN apt-get -yqq update                                                                                                                                               0.0s
 => CACHED [ 3/10] RUN apt-get -yqq install python-pip python-dev curl gnupg                                                                                                             0.0s
 => CACHED [ 4/10] RUN curl -sL https://deb.nodesource.com/setup_10.x | bash                                                                                                             0.0s
 => CACHED [ 5/10] RUN apt-get install -yq nodejs                                                                                                                                        0.0s
 => CACHED [ 6/10] ADD flask-app /opt/flask-app                                                                                                                                          0.0s
 => CACHED [ 7/10] WORKDIR /opt/flask-app                                                                                                                                                0.0s
 => CACHED [ 8/10] RUN npm install                                                                                                                                                       0.0s
 => CACHED [ 9/10] RUN npm run build                                                                                                                                                     0.0s
 => CACHED [10/10] RUN pip install -r requirements.txt                                                                                                                                   0.0s
 => exporting to image                                                                                                                                                                   0.0s
 => => exporting layers                                                                                                                                                                  0.0s
 => => writing image sha256:79842ea5bdcfeb794f7d26adf8da2491be9f9e76ac3be023126e8384dd4529dc                                                                                             0.0s
 => => naming to docker.io/yjbenlee/foodtrucks-web                                                                                                                                       0.0s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them
```

처음 실행할 때는 Docker 클라이언트가 우분투 이미지를 다운로드하고 모든 명령을 실행하고 이미지를 준비하기 때문에 시간이 다소 걸릴 수 있다. 이후 어플리케이션 코드를 변경한 후 도커 빌드를 다시 실행하면 거의 즉시 실행될 것이다. 이제 앱을 실행해 보자.

```bash
$ docker run -P --rm yjbenlee/foodtrucks-web
/usr/local/lib/python2.7/dist-packages/requests/__init__.py:91: RequestsDependencyWarning: urllib3 (1.26.16) or chardet (3.0.4) doesn't match a supported version!
  RequestsDependencyWarning)
Unable to connect to ES. Retrying in 5 secs...
Unable to connect to ES. Retrying in 5 secs...
Unable to connect to ES. Retrying in 5 secs...
Out of retries. Bailing out...
```

> 메시지 `/usr/local/lib/python2.7/dist-packages/requests/__init__.py:91: RequestsDependencyWarning: urllib3 (1.26.16) or chardet (3.0.4) doesn't match a supported version!
  RequestsDependencyWarning)`는 무시하여도 된다.

Oops! flask 앱이 Elasticsearch에 연결할 수 없어 실행할 수 없다. 한 컨테이너에 다른 컨테이너에 대해 알려주고 서로 대화하도록 하려면 어떻게 해야 할까? 그 답을 다음 섹션에서 다룬다.

## Docker 네트워크
이러한 시나리오를 처리하기 위해 Docker가 제공하는 기능에 대해 이야기하기 전에 문제를 해결할 수 있는 방법을 알아볼 수 있는지 알아보겠다. 이 과정을 통해 우리가 공부할 특정 기능에 대한 이해를 높일 수 있기를 바란다.

자, 이제 `dicker conatiner ls`(`docker ps`와 동일)를 실행하여 어떤 결과가 나오는지 살펴보자.

```bash
$ docker container ls
CONTAINER ID   IMAGE                                                 COMMAND                  CREATED          STATUS          PORTS                                                                                  NAMES
6b9e70a4ca46   docker.elastic.co/elasticsearch/elasticsearch:6.3.2   "/usr/local/bin/dock…"   16 minutes ago   Up 16 minutes   0.0.0.0:9200->9200/tcp, :::9200->9200/tcp, 0.0.0.0:9300->9300/tcp, :::9300->9300/tcp   es
```

`0.0.0.0:9200` 포트에서 실행 중인 ES 컨테이너가 하나 있으며, 이 포트로 직접 액세스할 수 있다. Flask 앱을 이 URL에 연결하도록 지시할 수 있다면 ES에 연결하여 대화할 수 있어야 한다. Python 코드를 파헤쳐서 연결 세부 정보가 어떻게 정의되어 있는지 살펴보겠다.

```python
es = Elasticsearch(host='es')
```

이 작업을 수행하려면 ES 컨테이너가 `0.0.0.0` 호스트(기본 포트는 `9200`)에서 실행 중이라고 Flask 컨테이너에 알려주면 작동해야 한다. 안타깝게도 `0.0.0.0` IP는 호스트 머신, 즉 내 컴퓨터에서 ES 컨테이너에 액세스하기 위한 IP이므로 올바르지 않다. 다른 컨테이너는 동일한 IP 주소에서 이 컨테이너를 액세스할 수 없다. 해당 IP가 아니라면 어떤 IP 주소에서 ES 컨테이너에 액세스할 수 있을까? 

이제 Docker에서 네트워킹에 대한 탐구를 시작하기에 좋은 기회이다. Docker를 설치하면 자동으로 3개의 네트워크가 생성된다.

```bash
$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
bb86240f7806   bridge           bridge    local
678f04185f82   host             host      local
7fe510f062b3   none             null      local
```

브리지 네트워크(bridge network)는 컨테이너가 기본적으로 실행되는 네트워크이다. 즉, ES 컨테이너를 실행할 때 이 브리지 네트워크에서 실행되고 있었다는 뜻이다. 이를 확인하기 위해 네트워크를 검사해 보자.

```bash
$ docker network inspect bridge
[
    {
        "Name": "bridge",
        "Id": "bb86240f78066cc504c3a9d5b5205f0317efcdb8029fd408b99cbd2dde00e318",
        "Created": "2023-07-20T23:49:50.531008573Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "6b9e70a4ca46366dae38b6ab2616d5769217b1ac6d447d8c45f99f080e474cf2": {
                "Name": "es",
                "EndpointID": "e245c52c9f7048860653500ba9e12106e238c21456cfc6a70018226cea8364cb",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
```

출력에서 `Containers` 섹션 아래에 컨테이너 `277451c15ec1(?)`이 있는 것을 볼 수 있다. 또한 이 컨테이너에 할당된 IP 주소인 `172.17.0.2`도 볼 수 있다. 이 주소가 우리가 찾고 있는 IP 주소일까? 플라스크 컨테이너를 실행하고 이 IP에 접속하여 확인해 보자.

```bash
$ docker run -it --rm yjbenlee/foodtrucks-web bash
root@03c42a1081bb:/opt/flask-app# curl 172.17.0.2:9200
{
  "name" : "59LgCW6",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "NYn3IzDcR0ywQKMk07m2cA",
  "version" : {
    "number" : "6.3.2",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "053779d",
    "build_date" : "2018-07-20T05:20:23.451332Z",
    "build_snapshot" : false,
    "lucene_version" : "7.3.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
root@03c42a1081bb:/opt/flask-app# exit
```

이쯤 되면 상당히 간단하다. 대화형 모드에서 `bash` 프로세스로 컨테이너를 시작한다. `--rm`은 작업이 완료되면 컨테이너가 정리되므로 일회성 명령을 실행할 때 편리한 플래그이다. `curl`을 시도하려면 먼저 설치해야 한다. 설치가 완료되면 `172.17.0.2:9200`에서 ES와 실제로 대화할 수 있음을 알 수 있다. 굉장하지요?

컨테이너가 서로 대화할 수 있는 방법을 알아냈지만, 이 접근 방식에는 여전히 두 가지 문제가 있다.
1. IP가 변경될 수 있는데 어떻게 Flask 컨테이너에 `es` 호스트 이름이 `172.17.0.2` 또는 다른 IP를 의미한다고 알릴 수 있을까?
2. *브리지* 네트워크는 기본적으로 모든 컨테이너가 공유하기 때문에 이 방법은 안전하지 않다. 네트워크를 어떻게 격리할 수 있을까?

다행히 Docker는 이러한 질문에 대한 훌륭한 해답을 가지고 있다. `docker network` 명령을 사용하여 네트워크를 격리하면서 자체 네트워크를 정의할 수 있다.

먼저 자체 네트워크를 만들어 보겠다.

```bash
$ docker network create foodtrucks-net
51aa2ef54918f0e479df6cb90e4039ee2bab0543d6ed3bc0b174c1f6d1ac1db6
```

```bash
$ docker network ls
NETWORK ID     NAME             DRIVER    SCOPE
bb86240f7806   bridge           bridge    local
51aa2ef54918   foodtrucks-net   bridge    local
678f04185f82   host             host      local
7fe510f062b3   none             null      local
```

`network create` 명령은 현재 우리에게 필요한 새 *브리지* 네트워크를 생성한다. Docker에서 브리지 네트워크는 소프트웨어 브리지를 사용하여 동일한 브리지 네트워크에 연결된 컨테이너가 통신할 수 있도록 하는 동시에 해당 브리지 네트워크에 연결되지 않은 컨테이너로부터 격리 기능을 제공한다. Docker 브리지 드라이버는 호스트 머신에 자동으로 규칙을 설치하여 서로 다른 브리지 네트워크에 있는 컨테이너가 서로 직접 통신할 수 없도록 한다. 생성할 수 있는 다른 종류의 네트워크가 있으며, 공식 [문서](https://docs.docker.com/engine/userguide/networking/dockernetworks/)에서 이에 대해 읽어보길 바란다.

이제 네트워크가 생겼으니 `--net` 플래그를 사용하여 이 네트워크 내에서 컨테이너를 시작할 수 있다. 그렇게 해보자. 하지만 먼저 같은 이름의 새 컨테이너를 시작하기 위해 브리지(기본) 네트워크에서 실행 중인 ES 컨테이너를 중지하고 제거하겠다.

```bash
$ docker container stop es
es
```

```bash
$ docker container rm es
es
```

```bash
$ docker run -d --name es --net foodtrucks-net -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:8.8.1
57da1d9e892c61d8d8768fb88cb584a77e1925cb14d40af3d3ffe630822cca8e
```

```bash
$ docker network inspect foodtrucks-net
[
    {
        "Name": "foodtrucks-net",
        "Id": "51aa2ef54918f0e479df6cb90e4039ee2bab0543d6ed3bc0b174c1f6d1ac1db6",
        "Created": "2023-07-25T06:35:06.396643574Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "d441a1b698f6417bfc1694a45946af2d8fa02a4e3b19cf406848be96a33bfc2e": {
                "Name": "es",
                "EndpointID": "e3694cba6ed93dfb4eb05d61d222b78f388ab5ed96034a1371584986ab443230",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
```

보시다시피, 이제 `es` 컨테이너가 브리지 네트워크 `foodtrucks-net` 내에서 실행되고 있다. 이제 `foodtrucks-net` 네트워크에서 실행하면 어떤 일이 발생하는지 살펴보겠다.

```bash
$ docker run -it --rm --net foodtrucks-net yjbenlee/foodtrucks-web bash
root@f6358b9fce22:/opt/flask-app# curl es:9200
{
  "name" : "3cBgyy6",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "n2I7FfYVSWKD0Gi_kMp8YQ",
  "version" : {
    "number" : "6.3.2",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "053779d",
    "build_date" : "2018-07-20T05:20:23.451332Z",
    "build_snapshot" : false,
    "lucene_version" : "7.3.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
root@f6358b9fce22:/opt/flask-app# ls
app.py  node_modules  package-lock.json  package.json  requirements.txt  static  templates  webpack.config.js
root@f6358b9fce22:/opt/flask-app# python app.py
/usr/local/lib/python2.7/dist-packages/requests/__init__.py:91: RequestsDependencyWarning: urllib3 (1.26.16) or chardet (3.0.4) doesn't match a supported version!
  RequestsDependencyWarning)
Index not found...
Loading data in elasticsearch ...
('Total trucks loaded: ', 481)
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```

Wohoo! 작동한다! 푸드트럭넷과 같은 사용자 정의 네트워크에서 컨테이너는 IP 주소로 통신할 수 있을 뿐만 아니라 컨테이너 이름을 IP 주소로 변환할 수도 있다. 이 기능을 *자동 서비스 검색(autimatic service recovery)*이라고 한다. 멋지네! 이제 Flask 컨테이너를 실제로 실행해 보자.

```bash
$ docker run -d --net foodtrucks-net -p 5000:5000 --name foodtrucks-web yjbenlee/foodtrucks-web
6c242c4f1500ba43b563460a0b79d03ecd1dcf2c8bafa371845016d266b9b19d
```

```bash
$ docker container ls
CONTAINER ID   IMAGE                                                 COMMAND                  CREATED          STATUS          PORTS                                                                                  NAMES
6c242c4f1500   yjbenlee/foodtrucks-web                               "python ./app.py"        12 seconds ago   Up 11 seconds   0.0.0.0:5000->5000/tcp, :::5000->5000/tcp                                              foodtrucks-web
d441a1b698f6   docker.elastic.co/elasticsearch/elasticsearch:6.3.2   "/usr/local/bin/dock…"   4 minutes ago    Up 4 minutes    0.0.0.0:9200->9200/tcp, :::9200->9200/tcp, 0.0.0.0:9300->9300/tcp, :::9300->9300/tcp   es
```

```bash
$ curl -I 0.0.0.0:5000
HTTP/1.0 200 OK
Content-Type: text/html; charset=utf-8
Content-Length: 3685
Server: Werkzeug/1.0.1 Python/2.7.17
Date: Wed, 26 Jul 2023 09:12:38 GMT

```

[http://0.0.0.0:5000](http://0.0.0.0:5000/) 로 이동하여 멋진 앱을 실시간으로 확인하세요! 많은 작업이 필요해 보이지만 실제로는 4개의 명령어만 입력하면 제로에서 실행까지 완료된다. 이 명령어들을 [bash 스크립트](https://github.com/prakhar1989/FoodTrucks/blob/master/setup-docker.sh)에 모아 두었다.

```bash
#!/bin/bash

# build the flask container
docker build -t yourusername/foodtrucks-web .

# create the network
docker network create foodtrucks-net

# start the ES container
docker run -d --name es --net foodtrucks-net -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.3.2

# start the flask app container
docker run -d --net foodtrucks-net -p 5000:5000 --name foodtrucks-web yourusername/foodtrucks-web
```

이제 앱을 친구에게 배포하거나 도커가 설치된 서버에서 실행하고 싶다면, 단 한 번의 명령으로 전체 앱을 실행할 수 있다!

```bash
$ git clone https://github.com/prakhar1989/FoodTrucks
$ cd FoodTrucks
$ ./setup-docker.sh
```

이게 다이다! 이것은 어플리케이션을 공유하고 실행하는 매우 훌륭하고 강력한 방법이라고 생각할 수 있다!

## Docker Compose
지금까지 주로 Docker 클라이언트를 살펴보는 데 시간을 보냈다. 그러나 Docker 생태계에는 Docker와 매우 잘 어울리는 다른 오픈 소스 도구가 많이 있다. 그 중 주요한 것은 다음과 같다.

- [Docker Machine](https://docs.docker.com/machine/) - 컴퓨터, 클라우드 제공업체 또는 자체 데이터 센터 내에서 Docker 호스트를 생성한다.
- [Docker Compose](https://docs.docker.com/compose/) - 멀티 컨테이너 Docker 애플리케이션을 정의하고 실행하기 위한 도구.
- [Docker Swarm](https://docs.docker.com/swarm/) - Docker를 위한 기본 클러스터링 솔루션
- [Kubernetes](https://kubernetes.io/) - 컨테이너화된 애플리케이션의 배포, 확장 및 관리를 자동화하기 위한 오픈 소스 시스템이다.

이 섹션에서는 이러한 도구 중 하나인 Docker Compose를 살펴보고 이를 통해 멀티 컨테이너 애플리케이션을 더 쉽게 처리할 수 있는 방법을 살펴본다.

Docker Compose의 배경 스토리는 꽤 흥미롭다. 2014년 1월경에 OrchardUp이라는 회사에서 Fig라는 도구를 출시했다. Fig의 아이디어는 격리된 개발 환경이 Docker와 함께 작동하도록 하는 것이었다. 이 프로젝트는 [Hacker News](https://news.ycombinator.com/item?id=7132044)에서 매우 호평을 받았다. 저자는 이상하게도 이 프로젝트에 대해 읽은 기억은 있지만 그 요령을 잘 이해하지 못했습니다.

포럼의 [첫 번째 댓글](https://news.ycombinator.com/item?id=7133449)은 실제로 Fig가 무엇인지 잘 설명해 주고 있다.

> 이 시점에서 Docker의 핵심은 바로 프로세스 실행이다. 이제 Docker는 컨테이너 간 공유 볼륨(디렉토리)(예: 실행 중인 이미지), 호스트에서 컨테이너로의 포워드 포트, 로그 표시 등 프로세스를 실행하기 위한 매우 풍부한 API를 제공한다.
> 
> 하지만 그게 모두이다. 현재 Docker는 프로세스 수준에 머물러 있다.
> 
> 여러 컨테이너를 오케스트레이션하여 하나의 "앱"을 생성하는 옵션은 제공하지만, 이러한 컨테이너 그룹을 단일 엔티티로 관리하는 것은 다루지 않고 있다. 그래서 Fig와 같은 도구는 컨테이너 그룹을 단일 엔티티로 다루는 데 유용하다. "컨테이너 실행" 대신 "앱 실행"(즉, "컨테이너의 오케스트레이션된 클러스터 실행")이라고 생각하자.

Docker를 사용하는 많은 사람들이 이러한 정서에 동의하였다. Fig가 서서히, 그리고 꾸준히 인기를 얻자 Docker는 이를 눈여겨보고 [회사를 인수한](https://www.docker.com/blog/welcoming-the-orchard-and-fig-team/) 후 Fig의 브랜드를 Docker Compose로 변경했다.

그렇다면 *Compose*를 어떤 용도로 사용할 수 있을까? Compose는 멀티 컨테이너 Docker 앱을 쉽게 정의하고 실행하는 데 사용되는 도구이다. 이 도구는 단 한 번의 명령으로 어플리케이션과 어플리케이션에 의존하는 서비스 제품군을 불러오는 데 사용할 수 있는 `docker-compose.yml`이라는 구성 파일을 제공한다. Compose는 개발과 테스트 환경에 이상적이지만, 프로덕션, 스테이징, 개발, 테스트, CI 워크플로우 등 모든 환경에서 작동한다.

SF-Foodtrucks 앱에 대한 `docker-compose.yml` 파일을 생성하고 Docker Compose가 그 약속에 부응하는지 평가해 보겠다.

첫 번째 단계는 Docker Compose를 설치하는 것이다. Windows 또는 Mac을 실행하는 경우, Docker Compose는 Docker 도구 상자에 포함되어 있으므로 이미 설치되어 있다. Linux 사용자는 문서에 있는 [지침](https://docs.docker.com/compose/install/)에 따라 쉽게 Docker Compose를 사용할 수 있다. Compose는 Python으로 작성되었기 때문에 `pip install docker-compose`만 수행하면 된다. 다음을 사용하여 설치를 테스트할 수 있다.

```bash
$ docker-compose --version
docker-compose version 1.29.2, build 5becea4c
```

이제 설치가 완료되었으므로 다음 단계, 즉 Docker Compose 파일인 `docker-compose.yml`로 넘어갈 수 있다. YAML의 구문은 매우 간단하며 리포지토리에 우리가 사용할 docker-compose [파일](https://github.com/prakhar1989/FoodTrucks/blob/master/docker-compose.yml)이 이미 포함되어 있다.

```yaml
ersion: "3"
services:
  es:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.3.2
    container_name: es
    environment:
      - discovery.type=single-node
    ports:
      - 9200:9200
    volumes:
      - esdata1:/usr/share/elasticsearch/data
  web:
    image: yourusername/foodtrucks-web
    command: python app.py
    depends_on:
      - es
    ports:
      - 5000:5000
    volumes:
      - ./flask-app:/opt/flask-app
volumes:
  esdata1:
    driver: local
```

위의 파일이 무엇을 의미하는지 분석해 보겠다. 상위 수준에서는 서비스 이름인 `es`와 `web`을 정의한다. `image` 매개 변수는 항상 필수이며, Docker에서 실행하려는 각 서비스에 대해 매개 변수를 추가할 수 있다. `es`의 경우, Elastic 레지스트리에서 사용할 수 있는 `elasticsearch` 이미지를 참조하면 된다. Flask 앱의 경우, 이 섹션의 시작 부분에서 빌드한 이미지를 참조한다.

`command`와 `port`같은 다른 매개변수는 컨테이너에 대한 자세한 정보를 제공한다. `volumes` 매개변수는 `web` 컨테이너에서 코드가 상주할 마운트 지점을 지정한다. 이 옵션은 순전히 선택 사항이며 로그 등에 액세스해야 하는 경우에 유용하다. 나중에 개발 중에 어떻게 유용하게 사용할 수 있는지 살펴보겠다. 이 파일이 지원하는 매개변수에 대해 자세히 알아보려면 [온라인 reference](https://docs.docker.com/compose/compose-file)를 참조하도록 한다. 또한 로드한 데이터가 재시작 사이에 지속되도록 `es` 컨테이너에 볼륨을 추가한다. 또한 `depends_on`을 지정하여 docker가 `web`보다 먼저 `es` 컨테이너를 시작하도록 지시한다. 이에 대한 자세한 내용은 [docker compose 문서](https://docs.docker.com/compose/compose-file/#depends_on)에서 확인할 수 있다.

> **Note**: 대부분의 Compose 명령을 실행하려면 `docker-compose.yml` 파일이 있는 디렉터리 안에서 명령을 수행하여야 한다.

이제 파일이 준비되었으니 `docker-compose`가 실제로 작동하는지 살펴보자. 하지만 시작하기 전에 포트와 이름을 사용할 수 있는지 확인해야 한다. 따라서 Flask와 ES 컨테이너가 실행 중이라면 이를 중지시킨다.

```bash
$ docker stop es foodtrucks-web
es
foodtrucks-web
```

```bash
$ docker rm es foodtrucks-web
es
foodtrucks-web
```

이제 `docker-compose`를 실행할 수 있다. `FoodTrucks` 디렉토리로 이동하여 `docker-compose up`를 실행한다.

```bash
$ docker-compose up

...

```

IP로 이동하여 앱을 실시간으로 확인한다. 정말 놀랍지 않나요? 몇 줄의 구성만으로 두 개의 Docker 컨테이너가 동시에 성공적으로 실행되고 있다. 서비스를 중지하고 분리 모드로 다시 실행해 보겠다.

```

...

$ docker-conpose -d

...

$ docker-compose ps

...

```

당연히 두 컨테이너가 모두 성공적으로 실행되는 것을 볼 수 있다. 이름은 어디에서 왔을까? Compose가 자동으로 생성한 이름이다. 하지만 *Compose*가 네트워크도 자동으로 생성할까? 좋은 질문이다! 알아보도록 하자.

먼저 서비스 실행을 중지한다. 명령 한 번으로 언제든지 다시 시작할 수 있다. 데이터 볼륨은 유지되므로 `docker-compose up`을 사용하여 동일한 데이터로 클러스터를 다시 시작할 수 있다. 클러스터와 데이터 볼륨을 삭제하려면 `docker-compose down -v`를 입력하면 된다.

```bash
$ docker-compose donwn -v

...

```

지난번에 만들었던 `foodtrucks` 네트워크도 삭제하여고 한다.

```bash
$ docker network rm foodtruck-net
$ docker network ls

...

```

OK! 이제 서비스를 다시 실행하여 *Compose*가 제대로 작동하는지 확인해 보자.

```bash
$ docker-compose up -d

...

```

```bash
$ docker container ls

...

```

지금까지는 괜찮다. 이제 네트워크가 생성되었는지 살펴볼 차례이다.

```bash
$ docker network ls

...

```

compose가 `foodtrucks_default`이라는 새 네트워크를 생성하고 해당 네트워크에 새 서비스를 모두 연결하여 각 서비스를 서로 검색할 수 있도록 한 것을 볼 수 있다. 서비스의 각 컨테이너는 기본 네트워크에 가입하고 해당 네트워크의 다른 컨테이너가 도달할 수 있으며 컨테이너 이름과 동일한 호스트 이름에서 검색할 수 있다.

```bash
$ docker ps

...

```

```bash
$ docker network inspect foodtrucks_default

...

```

## 개발 워크플로우
다음 장으로 넘어가기 전에 마지막으로 한 가지 더 다루고자 한다. 앞서 설명한 것처럼, docker-compose는 개발과 테스트에 정말 유용하다. 이제 개발 과정에서 더 쉽게 컴포즈를 구성할 수 있는 방법을 살펴보겠다.

이 튜토리얼에서는 이미 만들어진 도커 이미지로 작업했다. 이미지를 처음부터 빌드했지만 어플리케이션 코드는 아직 건드리지 않았고 대부분 Docker파일과 YAML 구성을 편집하는 것으로 제한했다. 한 가지 궁금한 점은 개발 중에 워크플로우는 어떨까? 모든 변경 사항에 대해 계속 Docker 이미지를 생성하고, 게시한 다음 실행하여 변경 사항이 예상대로 작동하는지 확인해야 할까? 매우 지루하게 들릴 것이다. 더 나은 방법은 없을까? 이 섹션에서는 바로 그 방법을 살펴보겠다.

방금 실행한 Foodturcks 앱을 어떻게 변경할 수 있는지 살펴보자. 앱이 실행 중인지 확인한다,

```bash
$ docker container ls

...

```

이제 이 앱을 변경하여 `/hello` 경로로 요청이 있을 때 `Hello world!` 메시지를 표시할 수 있는지 살펴보겠다. 현재 앱은 404로 응답한다.

```bash
$ curl -I 0.0.0.0:5000/hello

...

```

왜 이런 일이 발생할까요? 저희는 Flask 앱이므로 `app.py`([link](https://github.com/prakhar1989/FoodTrucks/blob/master/flask-app/app.py#L48-L64))에서 답을 찾을 수 있다. Flask에서 경로를 @app.route 구문으로 정의한다. 파일에서 `/`, `/debug`와 `/search`의 세 가지 경로만 정의되어 있는 것을 볼 수 있다. `/` route는 메인 앱을 렌더링하고, `debug` route는 일부 디버그 정보를 반환하는 데 사용되며, 마지막으로 `search`는 앱에서 elasticsearch를 쿼리하는 데 사용된다.

```bash
$ curl 0.0.0.0:5000/debug

...

```

이러한 맥락을 고려할 때 `hello`에 대한 새 경로를 추가하려면 어떻게 해야 할까? 짐작할 수 있을 것이다. 즐겨 사용하는 편집기에서 `flask-app/app.py`를 열고 다음과 같이 변경해 보겠다.

```python
@app.route('/')
def index():
  return render_template("index.html")

# add a new hello route
@app.route('/hello')
def hello():
  return "hello world!"
```

이제 다시 요청을 해 보겠다.

```bash
$ curl -I 0.0.0.0:5000/hello

...

```

안 되네요! 우리가 뭘 잘못했을까? `app.py`에서 변경을 수행했지만 이 파일은 우리 콤퓨터(또는 호스트 컴퓨터)에 있지만, Docker는 `yjbenlee/foodtrucks-web` 이미지를 기반으로 컨테이너를 실행하고 있기 때문에 이 변경 사항이 반영되지 않았다. 이를 검증하기 위해 다음을 시도해 보자.

```bash
$ docker-compose run web bsah

...

```

여기서 하려는 작업은 컨테이너에서 실행 중인 `app.py`에 변경 사항이 없는지 확인하는 것이다. 이 작업은 사촌인 `docker run`과 유사하지만 서비스에 대한 추가 인수(이 [경우](https://github.com/prakhar1989/FoodTrucks/blob/master/docker-compose.yml#L12) `web`)로 `docker-compose run` 명령을 실행하여 수행한다. `bash`를 실행하자마자 [Dockerfile](https://github.com/prakhar1989/FoodTrucks/blob/master/Dockerfile#L13)에 지정된 대로 `/opt/flask-app`에서 셸이 열린다. grep 명령에서 변경 사항이 파일에 없다는 것을 알 수 있다.

이 문제를 어떻게 해결할 수 있는지 알아보겠다. 먼저, 이미지를 사용하지 않고 대신 로컬에서 파일을 사용하도록 docker compose에 지시해야 한다. 또한 디버그 모드를 `true`로 설정하여 `app.py`가 변경될 때 Flask가 서버를 다시 로드하도록 알 수 있도록 한다. `docker-compose.yml` 파일의 `web` 부분을 다음과 같이 변경한다.

```yaml
version: "3"
services:
  es:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.3.2
    container_name: es
    environment:
      - discovery.type=single-node
    ports:
      - 9200:9200
    volumes:
      - esdata1:/usr/share/elasticsearch/data
  web:
    build: . # replaced image with build
    command: python3 app.py
    environment:
      - DEBUG=True # set an env var for flask
    depends_on:
      - es
    ports:
      - "5000:5000"
    volumes:
      - ./flask-app:/opt/flask-app
volumes:
  esdata1:
    driver: local
```

이 변경([diff](https://github.com/prakhar1989/FoodTrucks/commit/31368936de5959efaf4457a94c678d21e3eefbce))을 통해 컨테이너를 중지하고 시작해 보겠습니다.

```bash
$ docker-compose down -v

...

```

```bash
$ docker-compose up -d

...

```

마지막 단계로 `app.py`에 새 경로를 추가하여 변경해 보겠다.

```bash
$ curl -I 0.0.0.0:5000/hello

...

```

Wohoo! 유효한 응답을 받았다! 앱에서 더 많은 변경을 시도해 보세요.

이것으로 Docker Compose에 대한 설명을 마쳤다. Docker Compose를 사용하면 서비스를 일시 중지하고, 컨테이너에서 일회성 명령을 실행하고, 컨테이너의 수를 확장할 수도 있다. Docker Compose의 다른 몇 가지 [사용 사례](https://docs.docker.com/compose/overview/#common-use-cases)도 확인해 보자. Compose로 멀티 컨테이너 환경을 관리하는 것이 얼마나 쉬운지 보여드릴 수 있었다. 마지막 섹션에서는 AWS에 앱을 배포해 보겠다!

## AWS Elastic 컨테이너 서비스 
이 섹션은 AWS에 배포하여 서비스를 제공하는 것이므로 필요시 편역하도록 한다.
