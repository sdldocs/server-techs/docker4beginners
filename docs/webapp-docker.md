`docker run`을 살펴보았고, Docker 컨테이너를 사용해보고, 몇 가지 용어를 익혔다. 이 모든 지식으로 무장했으니 이제 실제 작업, 즉 Docker로 웹 어플리케이션을 배포할 준비가 되었다!

## 정적 사이트
걸음마 단계부터 시작하겠다. 가장 먼저 살펴볼 것은 아주 단순한 정적 웹사이트를 실행하는 방법이다. Docker Hub에서 Docker 이미지를 가져와 컨테이너를 실행하고 웹서버를 실행하는 것이 얼마나 쉬운지 살펴보도록 하자.

사용할 이미지는 이 데모를 위해 이미 만들어서 [레지스트리](https://hub.docker.com/r/prakhar1989/static-site/)에 호스팅하는 단일 페이지 [웹 사이트](http://github.com/prakhar1989/docker-curriculum) `prakhar1989/static-site`이다. `docker run`을 사용하여 한 번에 이미지를 직접 다운로드하고 실행할 수 있다. 위에서 언급했듯이 `--rm` 플래그는 컨테이너가 종료될 때 자동으로 컨테이너를 제거하며, `-it` 플래그는 대화형 터미널을 지정하여 `Ctrl+C`(윈도우의 경우)로 컨테이너를 쉽게 종료할 수 있도록 한다.

```bash
$ docker run --rm -it prakhar1989/static-site
```

이미지가 로컬에 존재하지 않으므로 클라이언트는 먼저 레지스트리에서 이미지를 가져온 다음 이미지를 실행한다. 모든 것이 순조롭게 진행되면 터미널에 `Nginx is running...` 메시지가 표시된다. 이제 서버가 실행 중이니 웹사이트를 어떻게 볼 수 있을까? 어떤 포트에서 실행되고 있을까? 그리고 더 중요한 것은 호스트 머신에서 컨테이너에 직접 액세스하려면 어떻게 해야 할까? 컨테이너를 중지하려면 `Ctrl+C`를 누른다.

이 경우 클라이언트가 포트를 노출하지 않으므로 포트를 게시하려면 `docker run` 명령을 다시 실행해야 한다. 이 과정에서 터미널이 실행 중인 컨테이너에 연결되지 않도록 하는 방법도 찾아야 한다. 이렇게 하면 터미널을 닫고 컨테이너를 계속 실행할 수 있다. 이를 분리 모드(detached mode)라고 한다.

```bash
$ docker run -d -P --name static-site prakhar1989/static-site
30e8af7420d8bf1943150fefe58af2e1a6aeb18f26381c7fb177aba7b4b39cfc
```

위의 명령에서 `-d`는 터미널을 분리하고, `-P`는 노출된 모든 포트를 임의의 포트로 게시하며, 마지막으로 `--name`은 우리가 지정하려는 이름에 해당한다. 이제 `docker port [CONTAINER]` 명령을 실행하여 포트를 확인할 수 있다.

```bash
$ docker port static-site
80/tcp -> 0.0.0.0:32769
443/tcp -> 0.0.0.0:32768
```

브라우저에서 `http://localhost:32769`로 열 수 있다.

> **Note**: docker-toolbox를 사용하는 경우 `docker-machine ip default`를 사용하여 IP를 가져올 수 있다.

클라이언트가 컨테이너에 대한 연결을 전달할 커스텀 포트를 지정할 수 있다.

```bash
$ docker run --platform linux/amd64 -p 8888:80 prakhar1989/static-site
Nginx is running...
```

> `--platform linux/amd64` 옵션은 MacBook Silicon에서 수행하기 위한 것이다.

![](images/webapp-docker/screenshot_05.png)

분리된 컨테이너를 중지시키려면 컨테이너 ID를 제공하여 `docker stop`를 실행한다. 이 경우 컨테이너를 시작할 때 사용한 `static-site`라는 이름을 사용할 수 있다.

```bash
$ docker stop static-site
static-site
```

매우 간단하다. 실제 서버에 배포하려면 Docker를 설치하고 위의 Docker 명령을 실행하기만 하면 된다. 이제 Docker 이미지 내에서 웹서버를 실행하는 방법을 살펴보았으니, 여러분만의 Docker 이미지는 어떻게 만들 수 있을까? 이것이 바로 다음 섹션에서 살펴볼 문제이다.

## Docker 이미지
이전에 이미지에 대해 살펴보았지만, 이번 섹션에서는 Docker 이미지가 무엇인지 더 자세히 알아보고 직접 이미지를 빌드해 보도록 한다. 마지막으로, 이 이미지를 사용하여 로컬에서 어플리케이션을 실행하고 마지막으로 AWS에 배포하여 친구들과 공유해 보겠다! 흥미롭겠지요? 이제 시작하자.(참고 [Build your Python image](https://docs.docker.com/language/python/build-images/))

Docker 이미지는 컨테이너의 기본입니다. 이전 예제에서는 레지스트리에서 *Busybox* 이미지를 가져와서 Docker 클라이언트에 해당 이미지를 기반으로 컨테이너를 실행하도록 요청했다. 로컬에서 사용할 수 있는 이미지 목록을 보려면 `docker images` 명령을 사용한다.

```bash
$ docker images
REPOSITORY                TAG       IMAGE ID       CREATED          SIZE
busybox                   latest    3fbc63216742   16 hours ago     6.09MB
hello-world               latest    926fac19d22a   17 hours ago     21.6kB
prakhar1989/static-site   latest    bb6907c8db9a   21 minutes ago   209MB
```

위는 저자가 직접 만든 이미지와 함께 레지스트리에서 가져온 이미지 목록니다(곧 방법을 살펴 볼 것이다). `TAG`는 이미지의 특정 스냅샷을 가리키고 `IMAGE ID`는 해당 이미지에 해당하는 고유 식별자이다.

쉽게 설명하기 위해 이미지를 git 리포지토리와 비슷하게 생각할 수 있다. 이미지는 변경 사항을 [commit](https://docs.docker.com/engine/reference/commandline/commit/)할 수 있고 여러 버전을 가질 수 있다. 특정 버전 번호를 제공하지 않으면 클라이언트는 기본적으로 최신 버전으로 설정한다. 예를 들어 특정 버전의 `ubuntu` 이미지를 가져올 수 있다.

```bash
$ docker pull ubuntu:18.04
```

새 Docker 이미지를 얻으려면 레지스트리(예: Docker Hub)에서 가져오거나 직접 만들 수 있다. [Docker Hub](https://hub.docker.com/explore/)에는 수만 개의 이미지가 있다. 명령어 `docker search`를 사용하여 이미지를 직접 검색할 수 있다.

이미지와 관련하여 알아야 할 중요한 차이점은 기본 이미지(base image)와 하위 이미지(child image)의 차이이다.
- 기본 이미지는 상위 이미지가 없는 이미지로, 일반적으로 ubuntu, busybox 또는 debian같이 OS로 사용되는 이미지이다.
- 하위 이미지는 기본 이미지를 기반으로 하여 추가 기능을 더하여 구축한 이미지이다.

그리고 공식(official) 이미지와 사용자(user) 이미지가 있으며, 이들은 기본 이미지 또는 하위 이미지가 될 수 있다.
- 공식 이미지는 Docker에서 공식적으로 유지 관리하고 지원하는 이미지이다. 일반적으로 한 단어 길이이다. 위의 이미지 목록에서 `ubuntu`, `busybox`와 `hello-world` 이미지는 공식 이미지이다.

- 사용자 이미지는 사용자가 만들고 공유하는 이미지이다. 기본 이미지를 기반으로 하여 추가 기능을 추가한 것이다. 일반적으로 `user/image-name` 형식으로 지정된다.

## 첫 이미지
이제 이미지에 대해 더 잘 이해했으니 이제 직접 이미지를 만들어 보도록 하자. 이 섹션의 목표는 간단한 [Flask](http://flask.pocoo.org/) 어플리케이션을 샌드박스하는 이미지를 만드는 것이다. 이 예제의 목적을 위해, 고양이를 좋아하지 않는 사람이 없으므로 로드할 때마다 임의의 고양이 `.gif`를 디스플레이하는 재미있는 작은 [Flask app](https://github.com/prakhar1989/docker-curriculum/tree/master/flask-app)을 만들었다. 다음과 같이 로컬에 저장소를 복제한다.

```bash
$ git clone https://github.com/prakhar1989/docker-curriculum.git
$ cd docker-curriculum/flask-app
```

> 도커 컨테이너 내부가 아닌 도커 명령을 실행하는 컴퓨터에 복제해야 한다.

다음 단계는 이 웹 앱으로 이미지를 만드는 것이다. 위에서 언급했듯이 모든 사용자 이미지는 기본 이미지를 기반으로 한다. 이 어플리케이션은 Python으로 작성되었으므로 기본 이미지는 [Python 3](https://hub.docker.com/_/python/)가 될 것이다.

## Dockerfile
[Dockerfile](https://docs.docker.com/engine/reference/builder/)은 이미지를 생성하는 동안 Docker 클라이언트가 호출하는 명령어 목록이 포함된 간단한 텍스트 파일이다. 이미지 생성 프로세스를 자동화하는 간단한 방법이다. 가장 좋은 점은 Dockerfile에 작성하는 [명령](https://docs.docker.com/engine/reference/builder/#from)이 해당 Linux 명령과 *거의 동일*하다는 것이다. 즉, 자신만의 도커파일을 만들기 위해 새로운 구문을 배울 필요가 없다.

어플리케이션 디렉토리에 Dockerfile이 포함되어 있지만 이 작업을 처음 수행하므로 처음부터 새로 만들어 보겠다. 시작하려면 자주 사용하는 텍스트 편집기에서 새 빈 파일을 만들고 flask 앱과 같은 폴더에 `Dockerfile`이라는 이름으로 저장한다.

기본 이미지를 지정하는 것부터 시작한다. 이를 위해 `FROM` 키워드를 사용한다.

```
FROM python:3.11
```

다음 단계는 일반적으로 파일을 복사하고 종속성을 설치하는 명령을 작성하는 것이다. 먼저 작업 디렉터리를 설정한 다음 앱의 모든 파일을 복사한다.

```
# set a directory for the app
WORKDIR /usr/src/app

# copy all the files to the container
COPY . .
```

이제 파일이 준비되었으므로 종속 요소를 설치할 수 있다.

```
# install dependencies
RUN pip install --no-cache-dir -r requirements.txt
```

다음으로 지정해야 할 것은 노출해야 하는 포트 번호이다. Flask 앱이 포트 5000에서 실행 중이므로 이 포트 번호를 지정한다.

```
EXPOSE 5000
```

마지막 단계는 어플리케이션을 실행하기 위한 명령어를 작성하는 것으로, 간단히 `python ./app.py`이다. 이를 위해 [CMD](https://docs.docker.com/engine/reference/builder/#cmd) 명령을 사용한다.

```
CMD ["python", "./app.py"]
```

CMD의 주요 목적은 컨테이너가 시작될 때 어떤 명령을 실행해야 하는지 컨테이너에 알려주는 것이다. 이제 Dockerfile을 준비되하였다. 다음과 같다.

```
FROM python:3.11

# set a directory for the app
WORKDIR /usr/src/app

# copy all the files to the container
COPY . .

# install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# define the port number the container should expose
EXPOSE 5000

# run the command
CMD ["python", "./app.py"]
```

이제 `Dockerfile`을 만들었니 이미지를 빌드할 수 있다. `docker build` 명령은 `Dockerfile`에서 Docker 이미지를 생성하는 무거운 작업을 수행한다.

아래 섹션에서는 동일한 명령을 실행한 결과를 보여준다. 명령을 직접 실행하기 전에(마침표를 잊지 마세요) `yourusername`을 여러분의 사용자 아이디로 바꿔야 한다. 이 사용자 이름은 [Docker hub](https://hub.docker.com/)에 등록할 때 만든 사용자 이름과 동일해야 한다. 아직 계정을 만들지 않았다면 지금 바로 계정을 만드세요. `docker build` 명령은 매우 간단하다. 선택 사항인 태그 이름에 `-t`를 붙이고 `Dockerfile`이 들어 있는 디렉터리 위치를 지정하면 된다.

```bash
docker build --platform linux/amd64 -t yjbenlee/catnip .
[+] Building 2.2s (7/8)                                                                                                                             docker:desktop-linux
[+] Building 2.4s (7/8)                                                                                                                             docker:desktop-linux
[+] Building 2.5s (7/8)                                                                                                                             docker:desktop-linux
 => [internal] load .dockerignore                                                                                                                                   0.0s
[+] Building 2.6s (7/8)                                                                                                                             docker:desktop-linux
[+] Building 2.8s (7/8)                                                                                                                             docker:desktop-linux
[+] Building 2.9s (7/8)                                                                                                                             docker:desktop-linux
[+] Building 3.8s (9/9) FINISHED                                                                                                                    docker:desktop-linux
 => [internal] load .dockerignore                                                                                                                                   0.0s
 => => transferring context: 2B                                                                                                                                     0.0s
 => [internal] load build definition from Dockerfile                                                                                                                0.0s
 => => transferring dockerfile: 344B                                                                                                                                0.0s
 => [internal] load metadata for docker.io/library/python:3.11                                                                                                      0.8s
 => [1/4] FROM docker.io/library/python:3.11@sha256:d73088ce13d5a1eec1dd05b47736041ae6921d08d2f240035d99642db98bc8d4                                                0.0s
 => => resolve docker.io/library/python:3.11@sha256:d73088ce13d5a1eec1dd05b47736041ae6921d08d2f240035d99642db98bc8d4                                                0.0s
 => [internal] load build context                                                                                                                                   0.0s  => => transferring context: 8.16kB                                                                                                                                 0.0s  => CACHED [2/4] WORKDIR /usr/src/app                                                                                                                               0.0s
 => [3/4] COPY . .                                                                                                                                                  0.0s
 => [4/4] RUN pip install --no-cache-dir -r requirements.txt                                                                                                        2.3s  => exporting to image                                                                                                                                              0.6s  => => exporting layers                                                                                                                                             0.5s
 => => exporting manifest sha256:46efcbdb1bee569ce898779e2f9642fec04309ff4c5244c815ba91063342c1cc                                                                   0.0s 
 => => exporting config sha256:861fcb888c98e048c6d0e1f30e0b6bd0511ebf94a06ba21d79f47ac1bbff46e4                                                                     0.0s 
 => => exporting attestation manifest sha256:de28a4befdc9edd10b5096e681d75c7105069206e95dae519937d4cfab1b2fb0                                                       0.0s 
 => => exporting manifest list sha256:e66c5012b594a17e5b536264d3cb4d614e9a5612e21521575b066ade789f1363                                                              0.0s 
 => => naming to docker.io/yjbenlee/catnip:latest                                                                                                                   0.0s
 => => unpacking to docker.io/yjbenlee/catnip:latest                                                                                                                0.2s

What's Next?
  View summary of image vulnerabilities and recommendations → docker scout quickview

```

`python:3.11` 이미지가 없는 경우 클라이언트가 먼저 이미지를 가져온 다음 이미지를 생성한다. 따라서 명령을 실행한 출력은 위와 다르게 보일 수 있다. 모든 것이 잘 되었다면 이미지가 준비되었을 것이다. `docker image`를 실행하여 이미지가 출력되는지 확인한다.

이 섹션의 마지막 단계는 이미지를 실행하여 실제로 작동하는지 확인하는 것이다(`yjbenlee`를 여러분의 사용자 아이디로 변경).

```bash
docker run -p 8888:5000 yjbenlee/catnip
 * Serving Flask app 'app'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on all addresses (0.0.0.0)
 * Running on http://127.0.0.1:5000
 * Running on http://172.17.0.2:5000
Press CTRL+C to quit
```

방금 실행한 명령은 컨테이너 내부의 서버에 포트 5000을 사용하고 포트 8888에서 이를 외부에 노출했다. 포트 8888이 있는 URL로 이동하면 앱이 라이브 상태여야 한다.

![](images/webapp-docker/screenshot_11.png)

축하합니다! 첫 번째 도커 이미지를 성공적으로 만들었다.

## Docker on AWS
친구들과 공유할 수 없는 어플리케이션이 무슨 소용이 있을까? 그래서 이 섹션에서는 멋진 어플리케이션을 클라우드에 배포하여 친구들과 공유할 수 있는 방법을 알아보겠다. 몇 번의 클릭만으로 어플리케이션을 시작하고 실행하기 위해 AWS [Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/)를 사용하겠다. 또한 Beanstalk를 통해 어플리케이션을 얼마나 쉽게 확장하고 관리할 수 있는지 살펴볼 것이다!

---

이후는 어디에서 컨테이너를 수행할 플랫폼에 따라 달라 일단 이 파트는 여기에서 중단하고 시간이 허락할 때 다시 계속할 것이다.
